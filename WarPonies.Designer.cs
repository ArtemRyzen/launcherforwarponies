﻿namespace LauncherForWarPonies
{
    sealed partial class WarPonies
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WarPonies));
            this.button1 = new System.Windows.Forms.Button();
            this.L_Progress = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TP_General = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.TSB_VkGroup = new System.Windows.Forms.ToolStripButton();
            this.TCB_OffSite = new System.Windows.Forms.ToolStripButton();
            this.TCB_RegistrationInGame = new System.Windows.Forms.ToolStripButton();
            this.L_StatusServer = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.L_SizeFile = new System.Windows.Forms.Label();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.label6 = new System.Windows.Forms.Label();
            this.L_SpeedDownload = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.L_Percent = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Информация = new System.Windows.Forms.Label();
            this.L_SizeMB = new System.Windows.Forms.Label();
#if Developers
            this.button6 = new System.Windows.Forms.Button();
#endif
            this.TP_Setting = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.TB_Gamepatch = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.B_Obzor = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.TM_CheckServer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.TP_General.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.TP_Setting.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(684, 246);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(155, 35);
            this.button1.TabIndex = 0;
            this.button1.Text = "Скачать игру";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // L_Progress
            // 
            this.L_Progress.AutoSize = true;
            this.L_Progress.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.L_Progress.Location = new System.Drawing.Point(288, 459);
            this.L_Progress.Name = "L_Progress";
            this.L_Progress.Size = new System.Drawing.Size(191, 22);
            this.L_Progress.TabIndex = 1;
            this.L_Progress.Text = "Прогресс скачивания";
            this.L_Progress.SizeChanged += new System.EventHandler(this.L_Progress_SizeChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(345, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(146, 31);
            this.label2.TabIndex = 2;
            this.label2.Text = "WarPonies";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(6, 484);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(832, 35);
            this.progressBar1.Step = 1;
            this.progressBar1.TabIndex = 3;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(3, 56);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(675, 390);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tabControl1.Controls.Add(this.TP_General);
            this.tabControl1.Controls.Add(this.TP_Setting);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(856, 479);
            this.tabControl1.TabIndex = 7;
            // 
            // TP_General
            // 
            this.TP_General.Controls.Add(this.groupBox4);
            this.TP_General.Controls.Add(this.toolStrip1);
            this.TP_General.Controls.Add(this.L_StatusServer);
            this.TP_General.Controls.Add(this.label5);
            this.TP_General.Controls.Add(this.richTextBox3);
            this.TP_General.Controls.Add(this.groupBox1);
#if Developers
            this.TP_General.Controls.Add(this.button6);
#endif
            this.TP_General.Controls.Add(this.pictureBox1);
            this.TP_General.Controls.Add(this.button1);
            this.TP_General.Controls.Add(this.label2);
            this.TP_General.Controls.Add(this.progressBar1);
            this.TP_General.Controls.Add(this.L_Progress);
            this.TP_General.Location = new System.Drawing.Point(4, 22);
            this.TP_General.Name = "TP_General";
            this.TP_General.Padding = new System.Windows.Forms.Padding(3);
            this.TP_General.Size = new System.Drawing.Size(848, 453);
            this.TP_General.TabIndex = 0;
            this.TP_General.Text = "WarPonies";
            this.TP_General.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button5);
            this.groupBox4.Controls.Add(this.button3);
            this.groupBox4.Controls.Add(this.button2);
            this.groupBox4.Location = new System.Drawing.Point(685, 287);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(152, 156);
            this.groupBox4.TabIndex = 20;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Панель управления";
            // 
            // button5
            // 
            this.button5.Enabled = false;
            this.button5.Location = new System.Drawing.Point(7, 61);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(139, 40);
            this.button5.TabIndex = 2;
            this.button5.Text = "Проверить целостность файлов";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.Location = new System.Drawing.Point(71, 19);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 35);
            this.button3.TabIndex = 1;
            this.button3.Text = "Удалить игру";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(7, 20);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(59, 34);
            this.button2.TabIndex = 0;
            this.button2.Text = "Отмена";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSB_VkGroup,
            this.TCB_OffSite,
            this.TCB_RegistrationInGame});
            this.toolStrip1.Location = new System.Drawing.Point(3, 3);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(842, 25);
            this.toolStrip1.TabIndex = 19;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // TSB_VkGroup
            // 
            this.TSB_VkGroup.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.TSB_VkGroup.Image = ((System.Drawing.Image)(resources.GetObject("TSB_VkGroup.Image")));
            this.TSB_VkGroup.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB_VkGroup.Name = "TSB_VkGroup";
            this.TSB_VkGroup.Size = new System.Drawing.Size(76, 22);
            this.TSB_VkGroup.Text = "Группа в ВК";
            this.TSB_VkGroup.Click += new System.EventHandler(this.TSB_VkGroup_Click);
            // 
            // TCB_OffSite
            // 
            this.TCB_OffSite.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.TCB_OffSite.Image = ((System.Drawing.Image)(resources.GetObject("TCB_OffSite.Image")));
            this.TCB_OffSite.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TCB_OffSite.Name = "TCB_OffSite";
            this.TCB_OffSite.Size = new System.Drawing.Size(119, 22);
            this.TCB_OffSite.Text = "Официальный сайт";
            this.TCB_OffSite.Click += new System.EventHandler(this.TCB_OffSite_Click);
            // 
            // TCB_RegistrationInGame
            // 
            this.TCB_RegistrationInGame.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.TCB_RegistrationInGame.Image = ((System.Drawing.Image)(resources.GetObject("TCB_RegistrationInGame.Image")));
            this.TCB_RegistrationInGame.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TCB_RegistrationInGame.Name = "TCB_RegistrationInGame";
            this.TCB_RegistrationInGame.Size = new System.Drawing.Size(117, 22);
            this.TCB_RegistrationInGame.Text = "Регистрация в игре";
            this.TCB_RegistrationInGame.Click += new System.EventHandler(this.TCB_RegistrationInGame_Click);
            // 
            // L_StatusServer
            // 
            this.L_StatusServer.AutoSize = true;
            this.L_StatusServer.Location = new System.Drawing.Point(779, 99);
            this.L_StatusServer.Name = "L_StatusServer";
            this.L_StatusServer.Size = new System.Drawing.Size(56, 13);
            this.L_StatusServer.TabIndex = 18;
            this.L_StatusServer.Text = "Узнаем...";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(688, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Статус сервера:";
            // 
            // richTextBox3
            // 
            this.richTextBox3.Location = new System.Drawing.Point(44, 169);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.Size = new System.Drawing.Size(339, 206);
            this.richTextBox3.TabIndex = 15;
            this.richTextBox3.Text = "";
            this.richTextBox3.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.L_SizeFile);
            this.groupBox1.Controls.Add(this.progressBar2);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.L_SpeedDownload);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.L_Percent);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Информация);
            this.groupBox1.Controls.Add(this.L_SizeMB);
            this.groupBox1.Location = new System.Drawing.Point(256, 525);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(333, 160);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Информация";
            // 
            // L_SizeFile
            // 
            this.L_SizeFile.AutoSize = true;
            this.L_SizeFile.Location = new System.Drawing.Point(7, 114);
            this.L_SizeFile.Name = "L_SizeFile";
            this.L_SizeFile.Size = new System.Drawing.Size(52, 13);
            this.L_SizeFile.TabIndex = 19;
            this.L_SizeFile.Text = "Размер: ";
            // 
            // progressBar2
            // 
            this.progressBar2.Location = new System.Drawing.Point(10, 130);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(317, 23);
            this.progressBar2.Step = 1;
            this.progressBar2.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar2.TabIndex = 18;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 95);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(135, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Идет скачивание файла: ";
            // 
            // L_SpeedDownload
            // 
            this.L_SpeedDownload.AutoSize = true;
            this.L_SpeedDownload.Location = new System.Drawing.Point(133, 69);
            this.L_SpeedDownload.Name = "L_SpeedDownload";
            this.L_SpeedDownload.Size = new System.Drawing.Size(35, 13);
            this.L_SpeedDownload.TabIndex = 16;
            this.L_SpeedDownload.Text = "label5";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Прогресс в процентах: ";
            // 
            // L_Percent
            // 
            this.L_Percent.AutoSize = true;
            this.L_Percent.Location = new System.Drawing.Point(133, 43);
            this.L_Percent.Name = "L_Percent";
            this.L_Percent.Size = new System.Drawing.Size(0, 13);
            this.L_Percent.TabIndex = 12;
            this.L_Percent.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Скорость скачивания:";
            // 
            // Информация
            // 
            this.Информация.AutoSize = true;
            this.Информация.Location = new System.Drawing.Point(7, 17);
            this.Информация.Name = "Информация";
            this.Информация.Size = new System.Drawing.Size(147, 13);
            this.Информация.TabIndex = 13;
            this.Информация.Text = "Размер скачанных файлов:";
            // 
            // L_SizeMB
            // 
            this.L_SizeMB.AutoSize = true;
            this.L_SizeMB.Location = new System.Drawing.Point(160, 17);
            this.L_SizeMB.Name = "L_SizeMB";
            this.L_SizeMB.Size = new System.Drawing.Size(55, 13);
            this.L_SizeMB.TabIndex = 10;
            this.L_SizeMB.Text = "L_SizeMB";
            this.L_SizeMB.TextChanged += new System.EventHandler(this.L_SizeMB_TextChanged);
            // 
            // button6
            // 
#if Developers
            this.button6.Location = new System.Drawing.Point(687, 56);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(150, 40);
            this.button6.TabIndex = 11;
            this.button6.Text = "Проверить деректорию лаунчера";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.Button6_Click);
#endif
            // 
            // TP_Setting
            // 
            this.TP_Setting.Controls.Add(this.groupBox2);
            this.TP_Setting.Location = new System.Drawing.Point(4, 22);
            this.TP_Setting.Name = "TP_Setting";
            this.TP_Setting.Padding = new System.Windows.Forms.Padding(3);
            this.TP_Setting.Size = new System.Drawing.Size(848, 453);
            this.TP_Setting.TabIndex = 1;
            this.TP_Setting.Text = "Настройки";
            this.TP_Setting.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.TB_Gamepatch);
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.B_Obzor);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(8, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(399, 287);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Лаунчер";
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(9, 74);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(382, 207);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Внешний вид";
            // 
            // TB_Gamepatch
            // 
            this.TB_Gamepatch.Location = new System.Drawing.Point(81, 19);
            this.TB_Gamepatch.Name = "TB_Gamepatch";
            this.TB_Gamepatch.ReadOnly = true;
            this.TB_Gamepatch.Size = new System.Drawing.Size(230, 20);
            this.TB_Gamepatch.TabIndex = 1;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(9, 46);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(183, 21);
            this.button4.TabIndex = 2;
            this.button4.Text = "Проверить обновления лаунчера";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // B_Obzor
            // 
            this.B_Obzor.Location = new System.Drawing.Point(317, 17);
            this.B_Obzor.Name = "B_Obzor";
            this.B_Obzor.Size = new System.Drawing.Size(74, 23);
            this.B_Obzor.TabIndex = 3;
            this.B_Obzor.Text = "Обзор";
            this.B_Obzor.UseVisualStyleBackColor = true;
            this.B_Obzor.Click += new System.EventHandler(this.B_Obzor_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Путь к игре:";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(874, 22);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(356, 184);
            this.richTextBox1.TabIndex = 8;
            this.richTextBox1.Text = "";
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(874, 225);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(356, 184);
            this.richTextBox2.TabIndex = 9;
            this.richTextBox2.Text = "";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // TM_CheckServer
            // 
            this.TM_CheckServer.Enabled = true;
            this.TM_CheckServer.Interval = 60000;
            this.TM_CheckServer.Tick += new System.EventHandler(this.TM_CheckServer_Tick);
            // 
            // WarPonies
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(851, 474);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WarPonies";
            this.Text = "Launcher";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.TP_General.ResumeLayout(false);
            this.TP_General.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.TP_Setting.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label L_Progress;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TP_General;
        private System.Windows.Forms.TabPage TP_Setting;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox TB_Gamepatch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button B_Obzor;
        private System.Windows.Forms.Label L_SizeMB;
        private System.Windows.Forms.RichTextBox richTextBox2;
#if Developers
        private System.Windows.Forms.Button button6;
#endif
        private System.Windows.Forms.Label L_Percent;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label Информация;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label L_SpeedDownload;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label L_StatusServer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Timer TM_CheckServer;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton TSB_VkGroup;
        private System.Windows.Forms.ToolStripButton TCB_OffSite;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ToolStripButton TCB_RegistrationInGame;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label L_SizeFile;
        public System.Windows.Forms.Label label2;
    }
}

