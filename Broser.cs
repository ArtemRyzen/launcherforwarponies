﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LauncherForWarPonies
{
    public partial class Broser : Form
    {
        public Broser()
        {
            InitializeComponent();
            
        }
        
        private void B_SelectedBrowser_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.Description = "Укажите дерикторию куда будет установлена игра";
            if(folderBrowserDialog1.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            
            string filename = folderBrowserDialog1.SelectedPath;
            TB_Gamepatch.Text = filename;
        }

        private void B_StartDownload_Click(object sender, EventArgs e)
        {
            if (TB_Gamepatch.Text == "")
            {
                MessageBox.Show("Вы не указали путь!");
            }
            else
            {
                Information.Default.GamePatch = TB_Gamepatch.Text;
                Information.Default.Save();
                Program.Filepatch.ClosinguserBrowserDiolog = false;
                this.Close();
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Program.Filepatch.ClosinguserBrowserDiolog = true;
            this.Close();
        }
    }
}
