﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace LauncherForWarPonies
{
    public partial class File1
    {
        [JsonProperty("response")]
        public Response Response { get; set; }
    }

    public partial class Response
    {
        [JsonProperty("items")]
        public Item[] Items { get; set; }
    }

    public partial class Item
    {
        [JsonProperty("file")]
        public string File { get; set; }

        [JsonProperty("path")]
        public string Path { get; set; }

        [JsonProperty("hash1")]
        public string Hash1 { get; set; }

        [JsonProperty("sizeFile")]
        public string Size { get; set; }
    }
}
