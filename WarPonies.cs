﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Threading;
using System.Diagnostics;
using System.Net.Sockets;
using System.IO.Compression;
using Newtonsoft.Json;
using System.Timers;

namespace LauncherForWarPonies
{
    public sealed partial class WarPonies : Form
    {
        private const int Port = 8888;

        private readonly string BaseIp;


        private readonly byte[] _downloadSpeed = new byte[10000000];
        private const bool UsePassive = true;
        private static readonly ManualResetEvent Mre = new ManualResetEvent(true);
        public bool ClosinguserBrowserDiolog = false;

        public WarPonies()
        {
#if Developers
            BaseIp = "127.0.0 .1";
#endif
            InitializeComponent();
            Program.Filepatch = this;
            TB_Gamepatch.Text = Information.Default.GamePatch + "\\WarPonies";
            //Иначе ,запускаем сканирование обновлений лаунчера
            Thread thread_1 = new Thread(new ThreadStart(CheckUpdateLauncher));
            thread_1.Name = "dd";
            thread_1.Start();

            Thread thread1 = new Thread(new ThreadStart(CheckStatusServer));
            thread1.Start();
        }

#if Developers
        private void DebugBox(string test)
        {
            MessageBox.Show(test);
        }
#endif
        private readonly List<Thread> _threads = new List<Thread>();

        private readonly List<string> _files = new List<string>();

        private readonly List<string> _fileName = new List<string>();

        private readonly List<string> _filePatch = new List<string>();

        private readonly List<string> _brokenFiles = new List<string>(); // Сюда записываются файлы которых нет, или которые

        private readonly List<string> _failFile = new List<string>(); // Сюда записываются файлы которые не прошли проверку

        long _timerInterval = 0; // Interval таймера равен 1000 (1 сек. = 1000 мс.)

        private void Button1_Click(object sender, EventArgs e)
        {
            string FilePacth = Information.Default.GamePatch;

            switch (button1.Text)
            {
                case "Приоcтановить":                 
                    //foreach(var items in threads)
                    //{
                    //    if(items.Name == "StartDownload")
                    //    {
                            
                    //    }
                    //}
                    Mre.Reset();
                    button1.Text = "Возобновить";
                    break;

                case "Возобновить":
                    Mre.Set();
                    button1.Text = "Приоcтановить";
                    break;

                case "Скачать игру":
                    Broser broser = new Broser();
                    broser.FormClosed += Broser_FormClosed;
                    broser.ShowDialog();
                    break;

                case "Обновить":
                    //MessageBox.Show("обновить!");
                    Thread thread = new Thread(new ThreadStart(UpdateFiles));
                    _threads.Add(thread);
                    thread.Start();
                    break;

                case "Запустить":
                    //MessageBox.Show("Запустить");
                    //Process.Start(FilePatch + "\\WarPonies\\MultiplayerShooter.exe");
                    System.Diagnostics.Process Proc = new System.Diagnostics.Process();
                    //MessageBox.Show(FilePacth);
                    Proc.StartInfo.FileName = FilePacth + @"\WarPonies\BinFiles\Binaries\Win64\WarPonies.exe";
                    Proc.StartInfo.Arguments = "/Game/Maps/Authorezation";
                    Proc.Start();
                    break;
            }
        }
        private void Broser_FormClosed(object sender, FormClosedEventArgs e)
        {
            Thread.Sleep(1000);
            if (ClosinguserBrowserDiolog == true)
            {
                //MessageBox.Show("Форма была закрыта пользователем");
            }
            else
            {
                //MessageBox.Show("Форма была закрыта приложением");
                Thread thread_StartDownload = new Thread(StartDownload);
                thread_StartDownload.Name = "StartDownload";
                _threads.Add(thread_StartDownload);
                thread_StartDownload.Start();
            }
        }
        private static string ReadFile(string filePatch)
        {
            if (filePatch == null) throw new ArgumentNullException(nameof(filePatch));
            using (FileStream fstream = File.OpenRead(filePatch))
            {
                // преобразуем строку в байты
                byte[] array = new byte[fstream.Length];
                // считываем данные
                fstream.Read(array, 0, array.Length);
                // декодируем байты в строку
                string textFromFile = System.Text.Encoding.Default.GetString(array);
                return textFromFile;
            }
        }
        public void DeleteFile(string directoryApp, string filePatch)
        {
            if (filePatch == null) throw new ArgumentNullException(nameof(filePatch));
            string path1 = directoryApp + filePatch;
            FileInfo fileInf1 = new FileInfo(path1);
            if (fileInf1.Exists)
            {
                //MessageBox.Show("Найден");
                fileInf1.Delete();
                //File.Move(FilePacth + "\\WarPonies\\NewFiles.txt", FilePacth + "\\WarPonies\\TXT.txt");
            }
        }

        //Проверка обновлений игры
        private void CheckUpdateGame()
        {

            //Проверяет существует ли манифест файлов
            //MessageBox.Show(File.Exists(Information.Default.GamePatch + "\\WarPonies\\TXT.txt").ToString());
            if (File.Exists(Information.Default.GamePatch + "\\WarPonies\\ManifestWarPonies.txt"))
            {
                //Если есть,то начинаем проверку файлов
                //Меняем вид формы
                this.InvokeIfNeeded(() => this.Size = new Size(870, 554));
                this.InvokeIfNeeded(() => progressBar1.Value = 0);
                this.InvokeIfNeeded(() => L_Progress.Text = "Проверка обновлений игры ");
                this.InvokeIfNeeded(() => this.Size = new Size(867, 585));


                //Скачивание нового текствого файла с данными о файлах, хеши и путях для проверки(Манифест)
                FtpWebRequest request2 = (FtpWebRequest)WebRequest.Create("ftp://" + BaseIp + "/WarPonies/ManifestWarPonies.txt");

                request2.Credentials = new NetworkCredential("WarPonies", "MJbvt0PQq");

                request2.Method = WebRequestMethods.Ftp.DownloadFile;

                FtpWebResponse response2 = (FtpWebResponse)request2.GetResponse();

                // получаем поток ответа
                Stream responseStream2 = response2.GetResponseStream();

                //Создание этого же файла в корневом каталоге игры
                FileStream fs = new FileStream(Information.Default.GamePatch + "\\WarPonies\\NewManifestWarPonies.txt", FileMode.Create, FileAccess.Write);
                byte[] buffer = new byte[64];
                int size = 0;
                StringBuilder builder = new StringBuilder();
                //Запись данных из потока в этот файл
                while ((size = responseStream2.Read(buffer, 0, buffer.Length)) > 0)
                {
                    fs.Write(buffer, 0, size);
                }
                //Закрытие потоков
                fs.Close();
                response2.Close();

                //Чтение файлов и запись структуры JSON в переменные
                string JSON_Server = ReadFile(Information.Default.GamePatch + "\\WarPonies\\NewManifestWarPonies.txt");
                string JSON_Client = ReadFile(Information.Default.GamePatch + "\\WarPonies\\ManifestWarPonies.txt");

#if Developers
                DebugBox(JSON_Server);
                DebugBox(JSON_Client);
#endif
                //Десериализация переменных

                File1 file1 = JsonConvert.DeserializeObject<File1>(JSON_Server);
                File1 file2 = JsonConvert.DeserializeObject<File1>(JSON_Client);

                //MessageBox.Show("fff");

                string[] HashClient = new string[file2.Response.Items.Length];
                int x = 0;
                //Запись хеша текущих файлов клиента в массив
                foreach (var Client in file2.Response.Items)
                {
                    HashClient[x] = Client.Hash1;
                    x++;
                }
                x = 0;
                this.InvokeIfNeeded(() => progressBar1.Maximum = file1.Response.Items.Length);
                foreach (var Server in file1.Response.Items)
                {

                    this.InvokeIfNeeded(() => richTextBox1.Text += "Проход цикла Server\n");
                    //Проверка существуют ли нужные папки в каталоге игры
                    if (File.Exists(Information.Default.GamePatch + "\\" + Server.Path + "\\" + Server.File))
                    {
                        this.InvokeIfNeeded(() => richTextBox1.Text += "Хеш на сервере: " + Server.Hash1 + "\n");
                        this.InvokeIfNeeded(() => richTextBox1.Text += "Хеш на Клиенте: " + HashClient[x] + "\n");
                        //Сравнение хеша клиента и хеша с сервера
                        if (Server.Hash1 != HashClient[x])
                        {
                            //Если хеш не совпадает добавляем в LIST string файлы которые нужно будет скачать
                            _files.Add(Information.Default.GamePatch + "\\" + Server.Path + "\\" + Server.File);
                            _fileName.Add(Server.File);
                            _filePatch.Add(Server.Path);
                        }
                        else
                        {

                        }
                    }
                    x++;
                    this.InvokeIfNeeded(() => progressBar1.PerformStep());

                }
                //MessageBox.Show(Files.Count.ToString());
                //MessageBox.Show(Files[0]);
                Thread.Sleep(400);
                //MessageBox.Show(Files.Count.ToString());
                
                if (_files.Count == 0)
                {
                    //Если обновлений в файлах не найденно, то можно даем пользователю возможность запустить игру
                    this.InvokeIfNeeded(() => button1.Text = "Запустить");
                    this.InvokeIfNeeded(() => button1.Enabled = true);
                    this.InvokeIfNeeded(() => L_Progress.Text = "Файлы игры в порядке.");
                }
                else
                {
                    //Иначе показываем пользователю кнопку "Обновить"
                    this.InvokeIfNeeded(() => button1.Text = "Обновить");
                    this.InvokeIfNeeded(() => button1.Enabled = true);
                    this.InvokeIfNeeded(() => L_Progress.Text = "Доступно обновления!");
                }
                string FilePacth = Information.Default.GamePatch;
                string path1 = FilePacth + "\\WarPonies\\ManifestWarPonies.txt";
                FileInfo fileInf1 = new FileInfo(path1);
                if (fileInf1.Exists)
                {
                    //MessageBox.Show("Найден");
                    fileInf1.Delete();
                    File.Move(FilePacth + "\\WarPonies\\NewManifestWarPonies.txt", FilePacth + "\\WarPonies\\ManifestWarPonies.txt");
                }
                //this.InvokeIfNeeded(() => this.Size = new Size(821, 511));

            }
            else
            {
                MessageBox.Show("Удален один из кретический важных файлов игры!" +
                    "\nПожалуйста, либо востановите его,либо переустановите игру.");
            }
        }
       
        //Функция проверки целостности файлов
        private void ChechIntegrityGameFiles()
        {
            this.InvokeIfNeeded(() => L_Progress.Text = "Проверка целостности файлов игры");
            this.InvokeIfNeeded(() => this.Size = new Size(867, 585));

            //Если у пользователя есть файл с манифестом 
            if (File.Exists(Information.Default.GamePatch + "\\WarPonies\\ManifestWarPonies.txt"))
            {
                _failFile.Clear();
                string JSON_Client = ReadFile(Information.Default.GamePatch + "\\WarPonies\\ManifestWarPonies.txt");
                //MessageBox.Show(JSON_Client);
                File1 file = JsonConvert.DeserializeObject<File1>(JSON_Client);

                int x = file.Response.Items.Length - 1;

                int y = 0;

                this.InvokeIfNeeded(() => progressBar1.Maximum = file.Response.Items.Length - 1);
                //Проверка на существование файлов
                foreach (var items in file.Response.Items)
                {
                    if (x == y)
                    {
                        //MessageBox.Show("Проверка проваленна" + Information.Default.GamePatch + "\\" + items.Path + "\\" + items.File);
                        break;
                    }
                    FileInfo fileInf = new FileInfo(Information.Default.GamePatch + "\\" + items.Path + "\\" + items.File);
                    if (!fileInf.Exists)
                    {
#if Developers
                        DebugBox(Information.Default.GamePatch + "\\" + items.Path + "\\" + items.File);
#endif
                        _failFile.Add(Information.Default.GamePatch + "\\" + items.Path + "\\" + items.File);
                    }
                    this.InvokeIfNeeded(() => progressBar1.PerformStep());
                    y++;
                }

                //MessageBox.Show(FailFile.Count.ToString());

                if (_failFile.Count != 0)
                {
                    //MessageBox.Show(FailFile.Count.ToString());
                    DialogResult dialogResult = MessageBox.Show(string.Format("Не найденны,или " +
                        "поврежденны {0} файлов.\nЧто будем делать?\n(Начать " +
                        "восстановление) - Да\n(Закрыть лаунчер) - Нет", _failFile.Count.ToString()), "Ошибка", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
#if Developers
                    DebugBox(_failFile[0]);
#endif
                    switch (dialogResult)
                    {
                        case DialogResult.No:
                            Application.ExitThread();
                            Application.Exit();
                            break;

                        case DialogResult.Yes:
                            var thread = new Thread(new ThreadStart(GameFileRecovery));
                            thread.Name = "проверка обновленирй лаунчера";
                            thread.Start();
                            break;
                    }
                }
                else
                {
                    //MessageBox.Show("Все файлы в порядке.");
                    Thread thread = new Thread(new ThreadStart(CheckUpdateGame));
                    _threads.Add(thread);
                    thread.Start();
                }

            }
            else
            {
                //Иначе говорим пользователю, что у него не найден один важный файл
                //И предлагаем ему запустить восстановление файлов
                var dialogResult = MessageBox.Show("Не найден один или несколько важных файлов игры!" +
                                                   "\n Вы хотите запустить восстановление файлов игры?.", "Ошибка!", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
               if (dialogResult == DialogResult.Yes)
               {
                   Thread thread = new Thread(new ThreadStart(GameFileRecovery));
                   thread.Name = "Восстановление игровх файлов";
                   thread.Start();

                }
            }
        }

        //Функция обновления файлов игры
        private void UpdateFiles()
        {
            double FileSize = 0;
            
            this.InvokeIfNeeded(() => progressBar1.Value = 0);
            this.InvokeIfNeeded(() => L_Progress.Text = "Обновления файлов: ");
            this.InvokeIfNeeded(() => progressBar1.Maximum = _files.Count);

            for (int x = 0; x < _files.Count; x++)
            {
                string path = _files[x];
                FileInfo fileInf = new FileInfo(path);
                if (fileInf.Exists)
                {
                    fileInf.Delete();

                    var FTP = FtpDownloadFile(_filePatch[x] + "/" + _fileName[x], "WarPonies", "MJbvt0PQq", false, Thread.CurrentThread);

                    FileStream fs1 = new FileStream(_files[x], FileMode.Create, FileAccess.Write);

                    int size1 = 0;
                    StringBuilder builder1 = new StringBuilder();

                    while ((size1 = FTP.Item1.Read(_downloadSpeed, 0, _downloadSpeed.Length)) > 0)
                    {
                        this.InvokeIfNeeded(() => richTextBox1.Text += "Размер буфера: " + _downloadSpeed.Length + "\n");
                        this.InvokeIfNeeded(() => richTextBox1.Text += "Переменная SIZE: " + size1 + "\n");
                        FileSize += size1;
                        fs1.Write(_downloadSpeed, 0, size1);

                        double KiloByte = Math.Round(FileSize / 1024, 2);
                        double MegaByte = Math.Round(FileSize / 1024 / 1024, 2);
                        double GigaByte = Math.Round(FileSize / 1024 / 1024 / 1024, 2);
                        double TerraByte = Math.Round(FileSize / 1024 / 1024 / 1024 / 1024, 2);

                        if (KiloByte >= 1 && MegaByte < 1)
                        {
                            this.InvokeIfNeeded(() => L_SizeMB.Text = string.Format("Загруженно: {0}/440мб", KiloByte.ToString() + " КБ"));
                        }
                        else if (MegaByte >= 1 && GigaByte < 1)
                        {
                            this.InvokeIfNeeded(() => L_SizeMB.Text = string.Format("Загруженно: {0}/440мб", MegaByte.ToString() + " МБ"));
                        }
                        else if (GigaByte >= 1 && TerraByte < 1)
                        {
                            this.InvokeIfNeeded(() => L_SizeMB.Text = string.Format("Загруженно: {0}/440мб", GigaByte.ToString() + " ГБ"));
                        }
                        else if (TerraByte >= 1)
                        {
                            this.InvokeIfNeeded(() => L_SizeMB.Text = string.Format("Загруженно: {0}/440мб", TerraByte.ToString() + " ТБ"));
                        }
                    }
                    fs1.Close();
                    FTP.Item1.Close();
                    FTP.Item2.Close();
                    this.InvokeIfNeeded(() => progressBar1.PerformStep());
                }
            }

            
            _files.Clear();
            _fileName.Clear();
            _filePatch.Clear();
            MessageBox.Show("Все файлы обновлены\nПриятной игры!");

            ChechIntegrityGameFiles();
        }

        //Функция скачивание файлов игры
        private void StartDownload()
        {
            this.InvokeIfNeeded(() => TB_Gamepatch.Text = Information.Default.GamePatch);
            this.InvokeIfNeeded(() => L_Progress.Text = "Скачивание файлов игры");
            this.InvokeIfNeeded(() => this.Size = new Size(871, 755));
            this.InvokeIfNeeded(() => button1.Text = "Приоcтановить");
            
            string FilePacth = Information.Default.GamePatch;
            
            // сохраняем файл в дисковой системе
            // создаем поток для сохранения файла
            //Проверяет,если нет файла с описанием папок и хеша,то скачиваем

            if (File.Exists(Information.Default.GamePatch + "\\WarPonies\\ManifestWarPonies.txt"))
            {
                //Если файл уже существует , то ничего не делаем
            }
            else
            {
                //Если файла не существует ,то скачиваем его и загружаем
                var FTP = FtpDownloadFile("WarPonies/ManifestWarPonies.txt", "WarPonies", "MJbvt0PQq", false, Thread.CurrentThread);
                //Создаем файл
                FileStream fs = new FileStream(FilePacth + "\\Files.txt", FileMode.Create, FileAccess.Write);
                int size = 0;
                StringBuilder builder = new StringBuilder();
                while ((size = FTP.Item1.Read(_downloadSpeed, 0, _downloadSpeed.Length)) > 0)
                {
                    fs.Write(_downloadSpeed, 0, size);
                }
                fs.Close();
                FTP.Item2.Close();
                //Открываем для чтения только что созданный файл
                using (FileStream fstream = File.OpenRead(@FilePacth + "\\Files.txt"))
                {
                    // преобразуем строку в байты
                    byte[] array = new byte[fstream.Length];
                    // считываем данные
                    fstream.Read(array, 0, array.Length);
                    // декодируем байты в строку
                    string textFromFile = System.Text.Encoding.Default.GetString(array); //Та самамя строка с JSON
                    //MessageBox.Show(textFromFile);
                    //richTextBox1.Text += string.Format("Текст из файла: {0}", textFromFile);
                    //Десериализуем 
                    //MessageBox.Show(textFromFile);
                    File1 file1 = JsonConvert.DeserializeObject<File1>(textFromFile);

                    this.InvokeIfNeeded(() => progressBar1.Maximum = file1.Response.Items.Length - 1);
                    int MaxPercent = file1.Response.Items.Length - 1;
                    this.InvokeIfNeeded(() => L_Percent.Visible = true);
                    int Iteracie = 0, b = 0;
                    double FileSize = 0;

                    this.InvokeIfNeeded(() => timer1.Enabled = true);

                    //MessageBox.Show("");
                    foreach (var items in file1.Response.Items)
                    {
                        try
                        {

                            this.InvokeIfNeeded(() => label6.Text = string.Format("Идет скачивание файла: {0}",items.File));

                            this.InvokeIfNeeded(() => L_SizeFile.Text = string.Format("Размер файла: {0}", items.Size));

                            Application.DoEvents();
                            
                            b = Iteracie * 100 / MaxPercent;

                            //this.InvokeIfNeeded(() => L_SpeedDownload.Text =  timer1.Enabled.ToString());
                            this.InvokeIfNeeded(() => L_Percent.Text = string.Format("{0}%/100%", b));


                            //Скачиваем файл 
                            //MessageBox.Show(items.Path + "\\" + items.File);
                            var FTP2 = FtpDownloadFile(items.Path + "\\" + items.File, "WarPonies", "MJbvt0PQq", false, Thread.CurrentThread);

                            string path = FilePacth;
                            string subpath = items.Path;
                            //Создаем деректорию для загрузки туда файла
                            DirectoryInfo dirInfo = new DirectoryInfo(path);
                            if (!dirInfo.Exists)
                            {
                                dirInfo.Create();
                            }
                            dirInfo.CreateSubdirectory(subpath);

                            FileStream fs1 = new FileStream(FilePacth + "\\" + items.Path + "\\" + items.File, FileMode.Create, FileAccess.Write);

                            int size1 = 0;
                            StringBuilder builder1 = new StringBuilder();
                            int d = 0;

                            while ((size1 = FTP2.Item1.Read(_downloadSpeed, 0, _downloadSpeed.Length)) > 0)
                            {
                                Mre.WaitOne();
                                d += size1;

                                if (_timerInterval == 2)
                                {
                                    double r = d / _timerInterval;

                                    double KiloByte = Math.Round(r / 1024, 2);
                                    double MegaByte = Math.Round(r / 1024 / 1024, 2);
                                    double GigaByte = Math.Round(r / 1024 / 1024 / 1024, 2);
                                    double TerraByte = Math.Round(r / 1024 / 1024 / 1024 / 1024, 2);

                                    if (KiloByte >= 1 && MegaByte < 1)
                                    {
                                        this.InvokeIfNeeded(() => L_SpeedDownload.Text = string.Format(KiloByte.ToString() + " КБ\\сек"));
                                    }
                                    else if (MegaByte >= 1 && GigaByte < 1)
                                    {
                                        this.InvokeIfNeeded(() => L_SpeedDownload.Text = string.Format(MegaByte.ToString() + " МБ\\сек"));
                                    }
                                    else if (GigaByte >= 1 && TerraByte < 1)
                                    {
                                        this.InvokeIfNeeded(() => L_SpeedDownload.Text = string.Format(GigaByte.ToString() + " ГБ\\сек"));
                                    }
                                    else if (TerraByte >= 1)
                                    {
                                        this.InvokeIfNeeded(() => L_SpeedDownload.Text = string.Format(TerraByte.ToString() + " ТБ\\сек"));
                                    }
                                    d = 0;
                                    _timerInterval = 0;
                                }

                                //Прибавление к переменной текщие байты файла
                                FileSize += size1;
                                //Запись в файл
                                fs1.Write(_downloadSpeed, 0, size1);
                                //Конвертация байтов
                                ConvertBytes(FileSize);
                            }

                            this.InvokeIfNeeded(() => progressBar1.PerformStep());
                            Iteracie++;
                            fs1.Close();
                            FTP2.Item2.Close();
                        }
                        catch
                        {

                        }
                    }
                }
            }

            //Удаление временного файла
            string path1 = FilePacth + "\\Files.txt";
            FileInfo fileInf = new FileInfo(path1);
            if (fileInf.Exists)
            {
                fileInf.Delete();
                // альтернатива с помощью класса File
                // File.Delete(path);
            }

            //MessageBox.Show(T.ToString());

            Thread thread = new Thread(new ThreadStart(ChechIntegrityGameFiles));
            thread.Name = "проверка обновленирй лаунчера";
            thread.Start();
        }

        //Функция проверки обновлений лаунчер
        private void CheckUpdateLauncher()
        {
            //Переменная содержит путь до лаунчера
            string LauncherPatch = Application.StartupPath;

            //Скачиваем временный файл для проверки лаунчера содержащий описание версии
            var FTP = FtpDownloadFile("WarPonies/LauncherVersion.txt", "WarPonies", "MJbvt0PQq", false, Thread.CurrentThread);

            //Создаем временный файл с описанием версии лаунчера
            FileStream fs1 = new FileStream(LauncherPatch + @"\tempVersion.txt", FileMode.Create, FileAccess.Write);

            //Записываем данный в файл
            int size1 = 0;
            StringBuilder builder1 = new StringBuilder();
            while ((size1 = FTP.Item1.Read(_downloadSpeed, 0, _downloadSpeed.Length)) > 0)
            {
                fs1.Write(_downloadSpeed, 0, size1);
            }
            fs1.Close();
            FTP.Item2.Close();

            //Считываем данные файла в переменную
            string File = ReadFile(LauncherPatch + @"\tempVersion.txt");

            FileInfo fileInf1 = new FileInfo(LauncherPatch + @"\tempVersion.txt");

            //Удаляем временный файл
            if (fileInf1.Exists)
            {
                fileInf1.Delete();
            }

            //Проверяем версию лаунчера и строку с текущей версией
            if (File != Information.Default.LauncherVersion)
            {
                //Если найдены не совпадения, запускааем обновления лаунчера
                MessageBox.Show("Ваша версия лаунчера устарела!\nПрограмма закроется.", "Критическая ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.InvokeIfNeeded(() => this.Close());
            }
            else
            {
                //Иначе смотрим, есть ли у пользователя игра
                if (Directory.Exists(Information.Default.GamePatch + "\\WarPonies"))
                {
                    //Если есть запускаем проверку файлов игры
                    DirectoryInfo dirInfo = new DirectoryInfo(Information.Default.GamePatch + "\\WarPonies");
                    if (dirInfo.Exists)
                    {
                        Thread threadChechIntegrityGameFiles = new Thread(new ThreadStart(ChechIntegrityGameFiles));
                        _threads.Add(threadChechIntegrityGameFiles);
                        threadChechIntegrityGameFiles.Start();
                    }
                }
                else
                {
                    /**
                     *  Если при проверке обновлений лаунчера, была не найдена деректория игры, то меняем
                     *  название кнопки на скачать игру
                     */
                    this.InvokeIfNeeded(() => button1.Text = "Скачать игру");
                    this.InvokeIfNeeded(() => button1.Enabled = true);
                }
            }

        }

        //Функция восстановление файлов игры
        private void GameFileRecovery()
        {
            this.InvokeIfNeeded(() => L_Progress.Text = "Восстановление файлов");
            this.InvokeIfNeeded(() => this.Size = new Size(867, 585));
            if (File.Exists(Information.Default.GamePatch + "\\WarPonies\\ManifestWarPonies.txt"))
            {
                //Если файла не существует, то скачиваем и смотри, какие файлы поврежденны

                //Скачивание нового текствого файла с данными о файлах, хеши и путях для проверки(Манифест)
                FtpWebRequest request2 = (FtpWebRequest)WebRequest.Create("ftp://" + BaseIp + "/WarPonies/ManifestWarPonies.txt");
                request2.Credentials = new NetworkCredential("WarPonies", "MJbvt0PQq");
                request2.Method = WebRequestMethods.Ftp.DownloadFile;
                FtpWebResponse response2 = (FtpWebResponse)request2.GetResponse();
               
                //Получаем поток ответа
                Stream responseStream2 = response2.GetResponseStream();

                //Создание этого же файла в корневом каталоге игры
                FileStream fs = new FileStream(Information.Default.GamePatch + "\\WarPonies\\NewManifestWarPonies.txt", FileMode.Create, FileAccess.Write);
                byte[] buffer = new byte[64];
                int size = 0;
                StringBuilder builder = new StringBuilder();
                //Запись данных из потока в этот файл
                while ((size = responseStream2.Read(buffer, 0, buffer.Length)) > 0)
                {
                    fs.Write(buffer, 0, size);
                }
                //Закрытие потоков
                fs.Close();
                response2.Close();
                string JSON_Server = ReadFile(Information.Default.GamePatch + "\\WarPonies\\NewManifestWarPonies.txt");
                File1 file1 = JsonConvert.DeserializeObject<File1>(JSON_Server);

#if Developers
                DebugBox(_failFile[0]);
                DebugBox(file1.Response.Items[0].Path);
#endif
                List<string> _files = new List<string>(1);
                List<string> _Pathfiles = new List<string>(1);
                string _Filepath = Information.Default.GamePatch;


                foreach (var items in file1.Response.Items)
                {
                    foreach (var t in _failFile)
                    {
                        string path = t;
                        string[] Array1 = path.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
                        if (items.File == Array1[Array1.Length - 1])
                        {
#if Developers
                            DebugBox("Совпадение! " + items.File);
#endif
                            _files.Add(items.File);
                            _Pathfiles.Add(items.Path);
                        }
                    }
                }

                //После проверки скачиваем нужные файлы
                for (int x = 0; x < _files.Count; x++)
                {
                    //MessageBox.Show(test);
                    var FTP2 = FtpDownloadFile(_Pathfiles[x] + "\\" + _files[x], "WarPonies", "MJbvt0PQq", false, Thread.CurrentThread);
                    
                    //Создаем деректорию для загрузки туда файла
                    DirectoryInfo dirInfo = new DirectoryInfo(Information.Default.GamePatch);
                    if (!dirInfo.Exists)
                    {
                        dirInfo.Create();
                    }
                    dirInfo.CreateSubdirectory(_Pathfiles[x]);
                    int size1 = 0;
                    StringBuilder builder1 = new StringBuilder();
                    int d = 0;

                    FileStream fs1 = new FileStream(Information.Default.GamePatch + "\\" + _Pathfiles[x] + "\\" + _files[x], FileMode.Create, FileAccess.Write);
                    while ((size1 = FTP2.Item1.Read(_downloadSpeed, 0, _downloadSpeed.Length)) > 0)
                    {
                        d += size1;
                        //Запись в файл
                        fs1.Write(_downloadSpeed, 0, size1);
                    }

                    fs1.Close();
                    FTP2.Item2.Close();
                }

                //Запускаем проверку файлов и обновлений
                Thread thread = new Thread(new ThreadStart(ChechIntegrityGameFiles));
                thread.Name = "Восстановление игровх файлов";
                thread.Start();
            }

        }

        private void ConvertBytes(double FileSize)
        {
            double KiloByte = Math.Round(FileSize / 1024, 2);
            double MegaByte = Math.Round(FileSize / 1024 / 1024, 2);
            double GigaByte = Math.Round(FileSize / 1024 / 1024 / 1024, 2);
            double TerraByte = Math.Round(FileSize / 1024 / 1024 / 1024 / 1024, 2);


            if (KiloByte >= 1 && MegaByte < 1)
            {
                this.InvokeIfNeeded(() => L_SizeMB.Text = string.Format("Загруженно: {0}/650мб", KiloByte.ToString() + " КБ"));
            }
            else if (MegaByte >= 1 && GigaByte < 1)
            {
                this.InvokeIfNeeded(() => L_SizeMB.Text = string.Format("Загруженно: {0}/650мб", MegaByte.ToString() + " МБ"));
            }
            else if (GigaByte >= 1 && TerraByte < 1)
            {
                this.InvokeIfNeeded(() => L_SizeMB.Text = string.Format("Загруженно: {0}/650мб", GigaByte.ToString() + " ГБ"));
            }
            else if (TerraByte >= 1)
            {
                this.InvokeIfNeeded(() => L_SizeMB.Text = string.Format("Загруженно: {0}/650мб", TerraByte.ToString() + " ТБ"));
            }
        }

        private Tuple<Stream, FtpWebResponse> FtpDownloadFile(string url, string login, string password, bool protocolSsl, Thread thread)
        {
            bool error = false;
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://" + BaseIp + "/" + url);// Создаем объект FtpWebRequest - он указывает на файл, который будет загружен.
                request.Credentials = new NetworkCredential(login, password);     //Указываем учётные данные(логин и пароль).
                request.UsePassive = UsePassive;
                
                request.EnableSsl = protocolSsl;                                  // Шифрованное подключение.
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                //request.Method = WebRequestMethods.Ftp.GetFileSize;// Устанавливаем метод на загрузку файлов с FTP-сервера.                                    // Создаем поток для загрузки файла.
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                //MessageBox.Show(response.StatusCode.ToString());                                                            // Получаем ответ от сервера в виде объекта FtpWebResponse.
                Stream responseStream = response.GetResponseStream(); 
                // Создаём поток, для загрузки данных с FTP-сервера.                                                                  // Закрываем поток, для загрузки данных с FTP-сервера.
                return Tuple.Create(responseStream, response);
            }
            //Обрабатываем подключение
            catch (Exception e)
            {
                error = true;
                Stream stream = null;
                FtpWebResponse ftpWebResponse = null;
                MessageBox.Show(e.Message.ToString() + "\nНевозможно продолжить работу,лаунчер закроется","Критическая ошибка!",MessageBoxButtons.OK,MessageBoxIcon.Error);
                //MessageBox.Show(thread.Name);
                //Прерываем поток
                thread.Abort();
                thread.Join(500);               
                
                return Tuple.Create(stream, ftpWebResponse);
            }
            finally
            {
                //Закрываем лаунчер
                if (error == true)
                {
                    this.InvokeIfNeeded(() => this.Close());
                }
            }
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Application.StartupPath);
        }

        private void L_Progress_SizeChanged(object sender, EventArgs e)
        {

            //MessageBox.Show("lol\n" + L_Progress.Width);

            //MessageBox.Show(progressBar1.Width);
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            _timerInterval++;
        }

        private void L_SizeMB_TextChanged(object sender, EventArgs e)
        {
            richTextBox3.Text = _timerInterval.ToString();
        }

        private void B_Obzor_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.Description = "Выберите деректорию с папкой игры (не папку игры , а папку с папкой игры)";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            DirectoryInfo directoryInfo = new DirectoryInfo(folderBrowserDialog1.SelectedPath + "\\WarPonies");
            if (!directoryInfo.Exists)
            {
                MessageBox.Show("В указанной вами деректории" +
                    " не найденно папки с игрой.\nЛибо вы еще не установили " +
                    "игру, либо папка переименована.","Ошибка",
                    MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            else
            {
                var filename = folderBrowserDialog1.SelectedPath;
                TB_Gamepatch.Text = filename;
                Information.Default.GamePatch = filename;
                Information.Default.Save();
                var thread = new Thread(new ThreadStart(ChechIntegrityGameFiles));
                thread.Start();
            }
        }

        private void B_OpenVkGroup_Click(object sender, EventArgs e)
        {
            
        }

        private void B_OpenOffSite_Click(object sender, EventArgs e)
        {
            Process.Start("http://26.69.4.155");
        }

        private void B_Registration_Click(object sender, EventArgs e)
        {
            Process.Start("http://26.69.4.155/RegistrationForm.php");
        }

        private void CheckStatusServer()
        {
            try
            {
                TcpClient tcpClient = new TcpClient();
                tcpClient.Connect("26.69.4.155", 8887);
                this.InvokeIfNeeded(() => L_StatusServer.Text = "Вкл.");
                tcpClient.Close();
            }
            catch
            {
                this.InvokeIfNeeded(() => L_StatusServer.Text = "Выкл.");
            }
        }

        private void TM_CheckServer_Tick(object sender, EventArgs e)
        {
            Thread thread1 = new Thread(new ThreadStart(CheckStatusServer));
            thread1.Start();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
           
        }

        private void TCB_OffSite_Click(object sender, EventArgs e)
        {
            Process.Start("http://26.69.4.155");
        }

        private void TSB_VkGroup_Click(object sender, EventArgs e)
        {
            Process.Start("https://vk.com/war_ponies");
        }

        private void TCB_RegistrationInGame_Click(object sender, EventArgs e)
        {
            DopForms.RegistrationForm registrationForm = new DopForms.RegistrationForm();
            registrationForm.ShowDialog();
        }
    }
    public static class ControlExtentions
    {
        public static void InvokeIfNeeded(this Control control, Action doit)
        {

            if (control.InvokeRequired)

                control.Invoke(doit);

            else

                doit();

        }
        public static void InvokeIfNeeded<T>(this Control control, Action<T> doit, T arg)
        {

            if (control.InvokeRequired)

                control.Invoke(doit, arg);

            else

                doit(arg);
        }
    }
}
