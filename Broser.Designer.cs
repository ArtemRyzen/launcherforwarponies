﻿namespace LauncherForWarPonies
{
    partial class Broser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TB_Gamepatch = new System.Windows.Forms.TextBox();
            this.B_SelectedBrowser = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.B_StartDownload = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Путь установки игры:";
            // 
            // TB_Gamepatch
            // 
            this.TB_Gamepatch.Location = new System.Drawing.Point(134, 10);
            this.TB_Gamepatch.Name = "TB_Gamepatch";
            this.TB_Gamepatch.ReadOnly = true;
            this.TB_Gamepatch.Size = new System.Drawing.Size(259, 20);
            this.TB_Gamepatch.TabIndex = 1;
            // 
            // B_SelectedBrowser
            // 
            this.B_SelectedBrowser.Location = new System.Drawing.Point(399, 7);
            this.B_SelectedBrowser.Name = "B_SelectedBrowser";
            this.B_SelectedBrowser.Size = new System.Drawing.Size(95, 23);
            this.B_SelectedBrowser.TabIndex = 2;
            this.B_SelectedBrowser.Text = "Указать ";
            this.B_SelectedBrowser.UseVisualStyleBackColor = true;
            this.B_SelectedBrowser.Click += new System.EventHandler(this.B_SelectedBrowser_Click);
            // 
            // B_StartDownload
            // 
            this.B_StartDownload.Location = new System.Drawing.Point(13, 39);
            this.B_StartDownload.Name = "B_StartDownload";
            this.B_StartDownload.Size = new System.Drawing.Size(235, 23);
            this.B_StartDownload.TabIndex = 3;
            this.B_StartDownload.Text = "Начать скачивание";
            this.B_StartDownload.UseVisualStyleBackColor = true;
            this.B_StartDownload.Click += new System.EventHandler(this.B_StartDownload_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(254, 39);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(240, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Выйти";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Broser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 68);
            this.ControlBox = false;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.B_StartDownload);
            this.Controls.Add(this.B_SelectedBrowser);
            this.Controls.Add(this.TB_Gamepatch);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Broser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Выбор пути";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TB_Gamepatch;
        private System.Windows.Forms.Button B_SelectedBrowser;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button B_StartDownload;
        private System.Windows.Forms.Button button1;
    }
}