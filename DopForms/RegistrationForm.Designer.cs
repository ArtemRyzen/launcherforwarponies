﻿namespace LauncherForWarPonies.DopForms
{
    partial class RegistrationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TB_Login = new System.Windows.Forms.TextBox();
            this.TB_Password = new System.Windows.Forms.TextBox();
            this.TB_Email = new System.Windows.Forms.TextBox();
            this.TB_FourmeName = new System.Windows.Forms.TextBox();
            this.B_Registartion = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Логин:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Пароль:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Email:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Имя на формуе:";
            // 
            // TB_Login
            // 
            this.TB_Login.Location = new System.Drawing.Point(60, 10);
            this.TB_Login.Name = "TB_Login";
            this.TB_Login.Size = new System.Drawing.Size(191, 20);
            this.TB_Login.TabIndex = 4;
            // 
            // TB_Password
            // 
            this.TB_Password.Location = new System.Drawing.Point(67, 37);
            this.TB_Password.Name = "TB_Password";
            this.TB_Password.Size = new System.Drawing.Size(184, 20);
            this.TB_Password.TabIndex = 5;
            // 
            // TB_Email
            // 
            this.TB_Email.Location = new System.Drawing.Point(54, 64);
            this.TB_Email.Name = "TB_Email";
            this.TB_Email.Size = new System.Drawing.Size(197, 20);
            this.TB_Email.TabIndex = 6;
            // 
            // TB_FourmeName
            // 
            this.TB_FourmeName.Location = new System.Drawing.Point(108, 88);
            this.TB_FourmeName.Name = "TB_FourmeName";
            this.TB_FourmeName.Size = new System.Drawing.Size(143, 20);
            this.TB_FourmeName.TabIndex = 7;
            // 
            // B_Registartion
            // 
            this.B_Registartion.Location = new System.Drawing.Point(13, 117);
            this.B_Registartion.Name = "B_Registartion";
            this.B_Registartion.Size = new System.Drawing.Size(236, 23);
            this.B_Registartion.TabIndex = 8;
            this.B_Registartion.Text = "Зарегистрироваться";
            this.B_Registartion.UseVisualStyleBackColor = true;
            // 
            // RegistrationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(261, 146);
            this.Controls.Add(this.B_Registartion);
            this.Controls.Add(this.TB_FourmeName);
            this.Controls.Add(this.TB_Email);
            this.Controls.Add(this.TB_Password);
            this.Controls.Add(this.TB_Login);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "RegistrationForm";
            this.Text = "Регистрация в игре";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TB_Login;
        private System.Windows.Forms.TextBox TB_Password;
        private System.Windows.Forms.TextBox TB_Email;
        private System.Windows.Forms.TextBox TB_FourmeName;
        private System.Windows.Forms.Button B_Registartion;
    }
}